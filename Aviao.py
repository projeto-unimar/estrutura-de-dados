class Queue:
  def __init__(self):
    self.elements = []
    
  def head(self):
    self.ultimo = len(self.elements) - 1
    print(f"Modelo: {self.elements[self.ultimo].getModelo()}")
    print(f"Número: {self.elements[self.ultimo].getEmpresa()}")
    print(f"Origem {self.elements[self.ultimo].getOrigem()}")
    print(f"Destino: {self.elements[self.ultimo].getDestino()}")
    print(f"Quantidade de passageiros {self.elements[self.ultimo].getQtdPassageiro()}")
    print(f"Número do VOO {self.elements[self.ultimo].getNumeroVOO()}")

  def isEmpty(self):
    return self.elements == []

  def enqueue(self, item):
    self.elements.insert(0, item)
    
  def dequeue(self):
    return self.elements.pop()
  
  def size(self):
    return len(self.elements)

  def getALLaviao(self):
    for i in self.elements[::1]:
      print(i.getNumeroVOO())
    
  
class Aviao:
  def __init__(self, modelo, empresa, origem, destino, qtdPassageiro, numeroVOO):

    self.modelo = modelo
    self.empresa = empresa
    self.origem = origem
    self.destino = destino
    self.qtdPassageiro = qtdPassageiro
    self.numeroVOO = numeroVOO

  def getModelo(self):
    return self.modelo

  def getEmpresa(self):
    return self.empresa

  def getOrigem(self):
    return self.origem

  def getDestino(self):
    return self.destino

  def getQtdPassageiro(self):
    return self.qtdPassageiro

  def getNumeroVOO(self):
    return self.numeroVOO
  

aeroporto = Queue()
av1 = Aviao("Modelo aviao 1", "Empresa aviao 1", "Marilia", "Osasco", 5, 5)
# av1.setDado()
av2 = Aviao("Modelo aviao 2", "Empresa aviao 2", "Osasco", "Bauro", 85, 55)
av3 = Aviao("Modelo aviao 3", "Empresa aviao 3", "Marilia", "Vera cruz", 15, 45)




aeroporto.enqueue(av1)
aeroporto.enqueue(av2)
aeroporto.enqueue(av3)

aeroporto.head()
aeroporto.getALLaviao()