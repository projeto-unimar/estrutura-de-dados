def fibo(n):
    if n == 0 :
        return 1
    elif n == 1:
        return 1
    else:
        return fibo(n-1) + fibo(n-2)
    
res = 6

for i in range(0, res):
    print(f"[{fibo(i)}]", end="")

print(fibo(6))

