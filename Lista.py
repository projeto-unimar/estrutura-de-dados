def somaLista(numeros):
    if len(numeros) == 1:
        return numeros[0]
    else:
        return numeros[0] + somaLista(numeros[1:]) 

numeros = [9, 5, -8, -3]
print(somaLista(numeros))